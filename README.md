# Task-Definition Format (Version 2.1)

This project defines a format that can be used to specify benchmark tasks,
such as used for benchmarking verifiers, testers, and solvers.

Previous versions: 
- [1.0](https://gitlab.com/sosy-lab/benchmarking/task-definition-format/-/tree/1.0)
- [2.0](https://gitlab.com/sosy-lab/benchmarking/task-definition-format/-/tree/2.0)

## Motivation

Evaluating verification tools requires the specification of verification tasks,
such that benchmarking executions become highly reproducible.
A verification task consists of a system to verify and a specification.
To verify the correctness of the answer of the benchmarked verifier,
the expected result must be known.

In the context of the benchmark collection [sv-benchmarks](https://github.com/sosy-lab/sv-benchmarks),
the above-mentioned three components were specified in the file name of the program
(for example `c/ntdrivers/floppy_true-unreach-call_true-valid-memsafety.i.cil.c`).
However, encoding the program, the specification (consisting of two properties in the above example),
and two expected results (one for each property) causes maintenance problems
in the benchmark repository.

The solution for keeping the verification task (system and specification)
and the expected result together, is to use an explicit task-definition format,
and the community around the competition on software verification (SV-COMP) proposed
to use the below YAML format.
The first version of this format was published with SV-COMP 2019 ([SV-COMP 2019 Report], Section 4).

[SV-COMP 2019 Report]: https://doi.org/10.1007/978-3-030-17502-3_9


## Format Description

The file must be in [YAML Format](http://yaml.org/).

Each task definition contains the following items:
  - `format_version` (string, mandatory, value "2.1"):
    version of the format of this file
  - `input_files` (string or list of strings with file-name patterns, mandatory, must match at least one file):
    represents a file or directory name, or a list of files or directory names,
    that the input for the tool consists of;
    relative paths are interpreted relative to this file
  - `required_files` (string or list of strings with file-name patterns, optional):
    represents a file or directory name, or a list of files or directory names,
    that the tool requires to be present;
    relative paths are interpreted relative to this file
  - `properties` (list of dictionaries, each dictionary with at least the key "property_file", optional):
    the properties that constitute the specification of a system,
    each consisting of the following items:
    - `property_file` (string with file-name pattern that matches exactly one file):
      file that contains a property definition
      (can be an arbitrary file, for example, common property files [for C][C-props] and [for Java][Java-props])
    - `expected_verdict` (boolean, optional): the intended result whether the property holds
    - `subproperty` (string, optional): a subproperty of the property that is violated
      in cases where the property is a conjunction of subproperties (for expected_verdict "false")
  - `options` (dictionary, optional): application-specific parameters
  - `additional_information` (dictionary, optional): extra information about the task

[C-props]: https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks/tree/svcomp24/c/properties
[Java-props]: https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks/tree/svcomp24/java/properties

Here is an example extract of the task-definition file from sv-benchmarks [c/list-properties/list-1.yml](https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks/blob/master/c/list-properties/list-1.yml):


```yaml
format_version: '2.1'

input_files: 'list-1.i'

properties:
  - property_file: ../properties/unreach-call.prp
    expected_verdict: true
  - property_file: ../properties/valid-memsafety.prp
    expected_verdict: false
    subproperty: valid-memtrack
  - property_file: ../properties/coverage-branches.prp
  - property_file: ../properties/coverage-conditions.prp
  - property_file: ../properties/coverage-statements.prp

options:
  language: C
  data_model: ILP32
```


## Applications

- [BenchExec](https://github.com/sosy-lab/benchexec/): Benchmarking tool kit that uses this format
- [SV-Benchmarks](https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks/): Collection of verification and test tasks using this format
- [SV-COMP](https://sv-comp.sosy-lab.org/) Evaluates verification tasks using this format
- [Test-Comp](https://test-comp.sosy-lab.org/) Evaluates test tasks using this format


## Change Log

For reading older versions of the format, we refer to the [tags list](https://gitlab.com/sosy-lab/benchmarking/task-definition-format/-/tags).

- [Version 2.1](https://gitlab.com/sosy-lab/benchmarking/task-definition-format/-/tree/2.1): Dictionary `additional_information` added
- [Version 2.0](https://gitlab.com/sosy-lab/benchmarking/task-definition-format/-/tree/2.0): Dictionary `options` added
- [Version 1.0](https://gitlab.com/sosy-lab/benchmarking/task-definition-format/-/tree/1.0): Initial version of the task-definition format

